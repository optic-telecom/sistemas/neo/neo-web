import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../global';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url: string;
  public token;
  public identity;
  constructor(
    public http: HttpClient
  ) {
    this.url = GLOBAL.url;
   }

  login(user): Observable<any>{
    let username = user.name;
    let password = user.password;
	return this.http.post(this.url+'auth/obtain_token/', {username, password});
  }
  getIdentity(){
		let identity = localStorage.getItem('identity');
		if (identity != 'undefined'){
			this.identity = identity;
		}else{
			this.identity = null;
		}
		return this.identity;
	}
	getToken(){
		let token = localStorage.getItem('token');
		if (token != 'undefined'){
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}
}

