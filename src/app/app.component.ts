import { Component, OnInit, DoCheck } from '@angular/core';
import { UserService } from './services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck{
  title = 'Neo';
  public identity;
  public token;

  constructor(
    private userService: UserService
  ){
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  } 
  ngOnInit(){
    console.log("app.component loaded");
  }
  ngDoCheck(){
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }
}






