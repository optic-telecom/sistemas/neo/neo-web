import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { User } from '../../models/User';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  public user: User;
  public token;
  public identity;
  constructor(
    private route: ActivatedRoute,
		private router: Router,
    private userService: UserService
    ) {
    this.user = new User(null,null,null,null);
   }

  ngOnInit() {
    this.logout();
  }

  onSubmit(form){
    //console.log(this.user)
    this.userService.login(this.user).subscribe(
			response => {
        //console.log(response.token);
        this.token = response.token;
        localStorage.setItem('token', this.token);
        let tokenInfo = this.getDecodedAccessToken(response.token); // decode token
        //let expireDate = tokenInfo.exp; // get token expiration dateTime
        console.log(tokenInfo.email); // show decoded token object in console
        console.log(tokenInfo); // show decoded token object in console
        this.identity = tokenInfo.username;
        localStorage.setItem('identity', this.identity);
        setTimeout(() => 
          {
            this.router.navigate(['home']);
          },
        2500);
			},
			error =>{
				console.log(<any>error);
			}
		)
  }
  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }
  logout(){
    console.log("hola")
		this.route.params.subscribe(params =>{
			let logout = +params['confirm'];
			console.log(logout);
			if(params['confirm'] == 'confirm'){
				localStorage.removeItem("identity");
				localStorage.removeItem("token");
				this.identity = null;
				this.token = null;
				this.router.navigate(['login']);
			}
		})
	}

}
