import { NgModule }             from '@angular/core';
import { MatToolbarModule }     from '@angular/material/toolbar';
import { MatIconModule }        from '@angular/material/icon';
import { MatFormFieldModule }   from '@angular/material/form-field'
import { MatSelectModule }      from '@angular/material/select'; 
import { MatInputModule }       from '@angular/material/input';
import { MatButtonModule }      from '@angular/material/button';

@NgModule({
  declarations: [],
  imports: [MatToolbarModule, MatIconModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatButtonModule],
  exports: [MatToolbarModule, MatIconModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatButtonModule],
})

export class MaterialModule { }
